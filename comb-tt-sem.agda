{-# OPTIONS --type-in-type #-}
open import Data.Unit

Ty : Set
Ty = Set

Tm : Ty → Set
Tm A = A 


U : Ty
U = Set

-- Tm U = Ty

infixr 5 _⇒_
_⇒_ : Ty → Ty → Ty
A ⇒ B = A → B

infixl 10 _$_
_$_ : {A B : Ty} → Tm (A ⇒ B) → Tm A → Tm B
f $ a = f a

K : {A B : Ty} → Tm (A ⇒ B ⇒ A)
K a b = a

S : {A B C : Ty} → Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
S f g x = f x (g x)

I : {A : Ty} → Tm (A ⇒ A)
I {A} = S $ K $ K {B = A}

arr : Tm (U ⇒ U ⇒ U)
arr = λ A B → A → B

-- arr $ A $ B = A ⇒ B

Pi : (A : Tm U) → Tm ((A ⇒ U) ⇒ U)
Pi A B = (x : A) → B x

-- λ A → arr (arr A U) U

pi : Tm (Pi U (S $ (S $ (K $ arr) $ S (S $ (K $ arr) $ I) (K $ U)) $ K U))
pi = λ A B → Pi A B

-- pi $ A $ B = (Pi A) $ B

-- KK : Tm (Pi U (λ A → 


-- define dep S,K...
