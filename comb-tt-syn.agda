{-# OPTIONS --type-in-type --rewriting #-}
module comb-tt-syn where

open import Relation.Binary.PropositionalEquality
{-# BUILTIN REWRITE _≡_ #-}

infixr 5 _⇒_
infixl 10 _$_

postulate

  Ty : Set
  Tm : Ty → Set

variable A B C : Ty

postulate

  U : Ty

  Tm-U : Tm U ≡ Ty

  {-# REWRITE Tm-U #-}

  _⇒_ : Ty → Ty → Ty

  _$_ : Tm (A ⇒ B) → Tm A → Tm B

  |⇒| : Tm (U ⇒ U ⇒ U)
  ⇒≡ : A ⇒ B ≡ |⇒| $ A $ B

  K : Tm (A ⇒ B ⇒ A)  
  K$ : {a : Tm A}{b : Tm B} → K {A} {B} $ a $ b ≡ a

  S : Tm ((A ⇒ B ⇒ C) ⇒ (A ⇒ B) ⇒ A ⇒ C)
  S$ : {f : Tm (A ⇒ B ⇒ C)}{g : Tm (A ⇒ B)}{a : Tm A}
       → S {A} {B} {C} $ f $ g $ a ≡ f $ a $ (g $ a)

I : {A : Ty} → Tm (A ⇒ A)
I {A} = S $ K $ K {B = A}

postulate

  Π₀ : (A : Ty) → Tm ((A ⇒ U) ⇒ U)
  |⇒|≡ : |⇒| $ A $ B ≡ Π₀ A $ (K $ B)

--  _$₀_ : 

--  |λ| : (Ty → Tm A) → Tm (U ⇒ A)
--t : Tm (U ⇒ U)
--t = |λ| (λ A →  |⇒| $ (|⇒| $ A $ U) $ U)
--t = S {B = U} $ |λ| (λ A → (|⇒| $ (|⇒| $ A $ U))) $ (K $ U)
--t = S {B = U} $ (S $ (K $ |⇒|) $ (|λ| (λ A → (|⇒| $ A $ U)))) $ (K $ U)
--t = S {B = U} $ (S $ (K $ |⇒|) $ (S $ |⇒| $ (K $ U))) $ (K $ U)

postulate



   Π : Tm (Π₀ U $ (S {B = U} $ (S $ (K $ |⇒|) $ (S $ |⇒| $ (K $ U))) $ (K $ U)))
   Π₀≡ : {B : Tm (A ⇒ U)} → Π₀ A $ B ≡ {!Π $ A!} -- Π $ A $ B

  
  
